Attribute VB_Name = "Main"
'@IgnoreModule ProcedureNotUsed, UnassignedVariableUsage
'@Folder("Logger")
Option Explicit


Private Sub LoggerFactoryTest()
    Dim logger As ILogger
    Dim defaultLoggerFactory As ILoggerFactory
    Set defaultLoggerFactory = loggerFactory
    
    Dim loggerType As Variant
    For Each loggerType In Array("Immediate", "Worksheet", "File")
        Set logger = defaultLoggerFactory.Create(loggerType:=loggerType)
        Debug.Print TypeName(logger.ClassName)
    
        logger.WriteLines "Immediate Logger Test"
        logger.WriteLines "Immediate Logger Test", 4
        logger.WriteLines "Immediate Logger Test", 8
        
        With WorksheetLogger.Create
            .WriteLines "Immediate Logger Test in With"
            .WriteLines "Immediate Logger Test in With", 4
            .WriteLines "Immediate Logger Test in With", 8
        End With
        
        logger.WriteLines "======================", 4
        logger.WriteLines Array("A", "B", 1.3, True, -7.4, Exp(1)), 8
        logger.WriteLines "======================", 4
    Next loggerType
End Sub


Private Sub WorksheetLoggerTest()
    Dim logger As ILogger
    Set logger = WorksheetLogger.Create
    Debug.Print TypeName(logger.ClassName)

    logger.WriteLines "Immediate Logger Test"
    logger.WriteLines "Immediate Logger Test", 4
    logger.WriteLines "Immediate Logger Test", 8
    
    With WorksheetLogger.Create
        .WriteLines "Immediate Logger Test in With"
        .WriteLines "Immediate Logger Test in With", 4
        .WriteLines "Immediate Logger Test in With", 8
    End With
    
    logger.WriteLines "======================", 4
    logger.WriteLines Array("A", "B", 1.3, True, -7.4, Exp(1)), 8
    logger.WriteLines "======================", 4
End Sub


Private Sub FileLoggerTest()
    Dim logger As ILogger
    Set logger = FileLogger.Create
    Debug.Print TypeName(logger.ClassName)

    logger.WriteLines "Immediate Logger Test"
    logger.WriteLines "Immediate Logger Test", 4
    logger.WriteLines "Immediate Logger Test", 8
    
    With FileLogger.Create
        .WriteLines "Immediate Logger Test in With"
        .WriteLines "Immediate Logger Test in With", 4
        .WriteLines "Immediate Logger Test in With", 8
    End With
    
    logger.WriteLines "======================", 4
    logger.WriteLines Array("A", "B", 1.3, True, -7.4, Exp(1)), 8
    logger.WriteLines "======================", 4
End Sub


Private Sub ImmediateLoggerTest()
    Dim logger As ILogger
    Set logger = ImmediateLogger.Create
    Debug.Print TypeName(logger.ClassName)
    
    logger.WriteLines "Immediate Logger Test"
    logger.WriteLines "Immediate Logger Test", 4
    logger.WriteLines "Immediate Logger Test", 8
    
    With ImmediateLogger.Create
        .WriteLines "Immediate Logger Test in With"
        .WriteLines "Immediate Logger Test in With", 4
        .WriteLines "Immediate Logger Test in With", 8
    End With
    
    logger.WriteLines "======================", 4
    logger.WriteLines Array("A", "B", 1.3, True, -7.4, Exp(1)), 8
    logger.WriteLines "======================", 4
End Sub

