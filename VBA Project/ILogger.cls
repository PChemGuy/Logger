VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ILogger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "Public interface for the Logger classes"
'@Folder("Logger.Abstract")
'@ModuleDescription("Public interface for the Logger classes")
'@IgnoreModule ParameterNotUsed
'@Interface
Option Explicit


'@Description("Creates a Logger instance.")
Public Function Create(Optional ByVal options As Variant = Empty) As ILogger
Attribute Create.VB_Description = "Creates a Logger instance."
End Function


'@Description("Returns the object itself. Useful to retrieve the With object variable in a With block.")
Public Property Get Self() As ILogger
Attribute Self.VB_Description = "Returns the object itself. Useful to retrieve the With object variable in a With block."
End Property


'@Description("Returns the object class.")
Public Property Get ClassName() As ILogger
Attribute ClassName.VB_Description = "Returns the object class."
End Property


'@Description("Returns private class fields. Must be called on the default instance.")
Public Property Get ClassFields() As Variant
Attribute ClassFields.VB_Description = "Returns private class fields. Must be called on the default instance."
End Property


'@Description("Writes a single line or an array to log")
Public Sub WriteLines(ByVal lines As Variant, Optional ByVal indent As Long = 0)
Attribute WriteLines.VB_Description = "Writes a single line or an array to log"
End Sub
