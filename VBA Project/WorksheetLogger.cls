VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "WorksheetLogger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "Performs logging to worksheet"
'@Folder("Logger")
'@ModuleDescription("Performs logging to worksheet")
'@IgnoreModule SetAssignmentWithIncompatibleObjectType, ProcedureNotUsed
'@PredeclaredId
Option Explicit


Implements ILogger


Private Type TWorksheetLogger
    logWorksheet As Excel.Worksheet
    Base As IBaseLogger
End Type
Private this As TWorksheetLogger


Private Sub Class_Initialize()
    If Me Is Me.ClassName Then
        Set this.logWorksheet = WorksheetLog
        this.logWorksheet.Range("A1").value = "Log"
    End If
End Sub


'@Description("Returns instance reference")
Public Property Get Self() As ILogger
Attribute Self.VB_Description = "Returns instance reference"
    Set Self = Me
End Property


'@Description("Returns class reference")
Public Property Get ClassName() As ILogger
Attribute ClassName.VB_Description = "Returns class reference"
    Set ClassName = WorksheetLogger
End Property


'@Ignore ParameterNotUsed
'@Description("Default factory")
Public Function Create(Optional ByVal options As Variant = Empty) As ILogger
Attribute Create.VB_Description = "Default factory"
    Guard.NonDefaultInstance Me 'Must be called on the default instance
    
    Dim result As WorksheetLogger
    Set result = New WorksheetLogger
    result.init options
    Set Create = result
End Function


'@Description("Custom constructor")
Public Sub init(Optional ByVal options As Variant = Empty)
Attribute init.VB_Description = "Custom constructor"
    If VarType(options) And vbArray = vbArray Then
        Debug.Assert (LBound(options, 1) = 0) And (UBound(options, 1) = 0)
        Set this.logWorksheet = options(0)
    Else
        Dim defaults As Variant
        defaults = Me.ClassName.ClassFields
        Debug.Assert VarType(defaults) And vbArray = vbArray
        Debug.Assert (LBound(defaults, 1) = 0) And (UBound(defaults, 1) = 0)
        Set this.logWorksheet = defaults(0)
    End If
    this.logWorksheet.Range("A1").value = "Log"
    Set this.Base = BaseLogger.Create
End Sub


'@Description("Returns default values.")
Public Property Get ClassFields() As Variant
Attribute ClassFields.VB_Description = "Returns default values."
    Guard.NonDefaultInstance Me 'Must be called on the default instance
    ClassFields = Array(this.logWorksheet)
End Property


'@Description("Default factory")
Private Function ILogger_Create(Optional ByVal options As Variant = Empty) As ILogger
Attribute ILogger_Create.VB_Description = "Default factory"
    Set ILogger_Create = WorksheetLogger.Create(options)
End Function


'@Description("Returns instance reference")
Private Property Get ILogger_Self() As ILogger
Attribute ILogger_Self.VB_Description = "Returns instance reference"
    Set ILogger_Self = Me
End Property


'@Description("Returns class reference")
Private Property Get ILogger_ClassName() As ILogger
Attribute ILogger_ClassName.VB_Description = "Returns class reference"
    Set ILogger_ClassName = WorksheetLogger
End Property


'@Description("Returns private class fields. Must be called on the default instance.")
Private Property Get ILogger_ClassFields() As Variant
Attribute ILogger_ClassFields.VB_Description = "Returns private class fields. Must be called on the default instance."
    ILogger_ClassFields = WorksheetLogger.ClassFields
End Property


'@Description("Writes a single line or an array to immediate")
Private Sub ILogger_WriteLines(ByVal lines As Variant, Optional ByVal indent As Long = 0)
Attribute ILogger_WriteLines.VB_Description = "Writes a single line or an array to immediate"
    this.Base.WriteToImmediate lines, indent
    Dim rowIndex As Long
    rowIndex = this.logWorksheet.Range("A1").CurrentRegion.Rows.Count + 1
    
    If (VarType(lines) And vbArray) = vbArray Then
        Dim buffer() As Variant
        Dim lineIndex As Long
        ReDim buffer(0 To UBound(lines) - LBound(lines), 0 To 0)
        For lineIndex = 0 To UBound(lines) - LBound(lines)
            buffer(lineIndex, 0) = Space(indent) + CStr(lines(lineIndex + LBound(lines)))
        Next lineIndex
        this.logWorksheet.Range("A" & CStr(rowIndex) & ":A" & CStr(rowIndex + UBound(buffer))).value = buffer
    Else
        this.logWorksheet.Range("A" & CStr(rowIndex)).value = Space(indent) + CStr(lines)
    End If
End Sub
