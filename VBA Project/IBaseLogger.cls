VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "IBaseLogger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "Public interface for the BaseLogger class."
'@Folder("Logger.Abstract")
'@ModuleDescription("Public interface for the BaseLogger class.")
'@IgnoreModule ParameterNotUsed
'@Interface
Option Explicit


'@Description("Creates a BaseLogger instance.")
Public Function Create(Optional ByVal options As Variant = Empty) As IBaseLogger
Attribute Create.VB_Description = "Creates a BaseLogger instance."
End Function


'@Description("Returns the object itself. Useful to retrieve the With object variable in a With block.")
Public Property Get Self() As IBaseLogger
Attribute Self.VB_Description = "Returns the object itself. Useful to retrieve the With object variable in a With block."
End Property


'@Description("Returns the object class.")
Public Property Get ClassName() As IBaseLogger
Attribute ClassName.VB_Description = "Returns the object class."
End Property


'@Description("Echoes a single line or an array to the immediate")
Public Sub WriteToImmediate(ByVal lines As Variant, Optional ByVal indent As Long = 0)
Attribute WriteToImmediate.VB_Description = "Echoes a single line or an array to the immediate"
End Sub
