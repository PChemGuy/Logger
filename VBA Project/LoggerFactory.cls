VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "LoggerFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "Abstract factory generating ILogger objects"
'@Folder("Logger")
'@ModuleDescription("Abstract factory generating ILogger objects")
'@PredeclaredId
Option Explicit

Implements ILoggerFactory

Private Function ILoggerFactory_Create(Optional ByVal loggerType As String = "Immediate", Optional ByVal options As Variant = Empty) As ILogger
    Select Case loggerType
        Case "Immediate"
            Set ILoggerFactory_Create = ImmediateLogger.Create(options)
        Case "Worksheet"
            Set ILoggerFactory_Create = WorksheetLogger.Create(options)
        Case "File"
            Set ILoggerFactory_Create = FileLogger.Create(options)
        Case Else
            Dim errorDetails As TError
            With errorDetails
                .number = ErrNo.UnknownClassErr
                .name = "UnknownClassErr"
                .source = "LoggerFactory"
                .description = "Unknown logger type <" & loggerType & "> specified. Available types are: Immediate, Worksheet, File."
                .message = .description
            End With
            RaiseError errorDetails
    End Select
End Function
