VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "BaseLogger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "An object intended to be used as a private instance field in a class implementing the ILogger interface (composition in place of inheritance)."
'@ModuleDescription("An object intended to be used as a private instance field in a class implementing the ILogger interface (composition in place of inheritance).")
'@Folder("Logger")
'@PredeclaredId
Option Explicit


Implements IBaseLogger


'@Ignore ParameterNotUsed
'@Description("Default factory")
Public Function Create(Optional ByVal options As Variant = Empty) As IBaseLogger
Attribute Create.VB_Description = "Default factory"
    Dim result As BaseLogger
    Set result = New BaseLogger
    Set Create = result
End Function


'@Description("Default factory")
Private Function IBaseLogger_Create(Optional ByVal options As Variant = Empty) As IBaseLogger
Attribute IBaseLogger_Create.VB_Description = "Default factory"
    Set IBaseLogger_Create = BaseLogger.Create(options)
End Function


'@Description("Returns instance reference")
Private Property Get IBaseLogger_Self() As IBaseLogger
Attribute IBaseLogger_Self.VB_Description = "Returns instance reference"
    Set IBaseLogger_Self = Me
End Property


'@Description("Returns class reference")
Private Property Get IBaseLogger_ClassName() As IBaseLogger
Attribute IBaseLogger_ClassName.VB_Description = "Returns class reference"
    Set IBaseLogger_ClassName = BaseLogger
End Property


'@Description("Echoes a single line or an array to the immediate")
Private Sub IBaseLogger_WriteToImmediate(ByVal lines As Variant, Optional ByVal indent As Long = 0)
Attribute IBaseLogger_WriteToImmediate.VB_Description = "Echoes a single line or an array to the immediate"
    Dim buffer As String
    If (VarType(lines) And vbArray) <> 0 Then
        buffer = Space(indent) + Join(lines, vbNewLine & Space(indent))
    Else
        buffer = Space(indent) + CStr(lines)
    End If
    Debug.Print buffer
End Sub
