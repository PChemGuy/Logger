VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ILoggerFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "An abstract factory that creates ILogger objects."
'@Folder("Logger.Abstract")
'@ModuleDescription("An abstract factory that creates ILogger objects.")
'@Interface
Option Explicit


'@Description("Creates a Logger instance.")
Public Function Create(Optional ByVal loggerType As String = "Immediate", Optional ByVal options As Variant = Empty) As ILogger
Attribute Create.VB_Description = "Creates a Logger instance."
End Function
