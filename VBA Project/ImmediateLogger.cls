VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ImmediateLogger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "Performs logging to immediate"
'@Folder("Logger")
'@ModuleDescription("Performs logging to immediate")
'@PredeclaredId
Option Explicit


Implements ILogger


'@Ignore ParameterNotUsed
'@Description("Default factory")
Public Function Create(Optional ByVal options As Variant = Empty) As ILogger
Attribute Create.VB_Description = "Default factory"
    Dim result As ImmediateLogger
    Set result = New ImmediateLogger
    Set Create = result
End Function


'@Description("Default factory")
Private Function ILogger_Create(Optional ByVal options As Variant = Empty) As ILogger
Attribute ILogger_Create.VB_Description = "Default factory"
    Set ILogger_Create = ImmediateLogger.Create(options)
End Function


'@Description("Returns instance reference")
Private Property Get ILogger_Self() As ILogger
Attribute ILogger_Self.VB_Description = "Returns instance reference"
    Set ILogger_Self = Me
End Property


'@Description("Returns class reference")
Private Property Get ILogger_ClassName() As ILogger
Attribute ILogger_ClassName.VB_Description = "Returns class reference"
    Set ILogger_ClassName = ImmediateLogger
End Property


'@Description("Returns private class fields. Must be called on the default instance.")
Private Property Get ILogger_ClassFields() As Variant
Attribute ILogger_ClassFields.VB_Description = "Returns private class fields. Must be called on the default instance."
    ILogger_ClassFields = Empty
End Property


'@Description("Writes a single line or an array to immediate")
Private Sub ILogger_WriteLines(ByVal lines As Variant, Optional ByVal indent As Long = 0)
Attribute ILogger_WriteLines.VB_Description = "Writes a single line or an array to immediate"
    Dim buffer As String
    If (VarType(lines) And vbArray) <> 0 Then
        buffer = Space(indent) + Join(lines, vbNewLine & Space(indent))
    Else
        buffer = Space(indent) + CStr(lines)
    End If
    Debug.Print buffer
End Sub
