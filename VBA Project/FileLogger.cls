VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "FileLogger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "Performs logging to file"
'@Folder("Logger")
'@ModuleDescription("Performs logging to file")
'@IgnoreModule ProcedureNotUsed
'@PredeclaredId
Option Explicit


Implements ILogger


Private Type TFileLogger
    logPath As String
    logName As String
    logPathName As String
    fileHandle As Long
    Base As IBaseLogger
End Type
Private this As TFileLogger


Private Sub Class_Initialize()
    If Me Is Me.ClassName Then
        Dim Shell As wshShell
        Set Shell = New wshShell
        this.logPath = Shell.ExpandEnvironmentStrings("%temp%")
        this.logName = "log-" & Mid$(CreateObject("Scriptlet.TypeLib").GUID, 2, 36) & ".log"
        this.logPathName = this.logPath & Application.PathSeparator & this.logName
    End If
End Sub


'@Description("Returns instance reference")
Public Property Get Self() As ILogger
Attribute Self.VB_Description = "Returns instance reference"
    Set Self = Me
End Property


'@Description("Returns class reference")
Public Property Get ClassName() As ILogger
Attribute ClassName.VB_Description = "Returns class reference"
    Set ClassName = FileLogger
End Property


'@Ignore ParameterNotUsed
'@Description("Default factory")
Public Function Create(Optional ByVal options As Variant = Empty) As ILogger
Attribute Create.VB_Description = "Default factory"
    Guard.NonDefaultInstance Me 'Must be called on the default instance
    
    Dim result As FileLogger
    Set result = New FileLogger
    result.init options
    Set Create = result
End Function


'@Description("Custom constructor")
Public Sub init(Optional ByVal options As Variant = Empty)
Attribute init.VB_Description = "Custom constructor"
    If VarType(options) And vbArray = vbArray Then
        Debug.Assert (LBound(options, 1) = 0) And (UBound(options, 1) = 1)
        this.logPath = CStr(options(0))
        this.logName = CStr(options(1))
    Else
        Dim defaults As Variant
        defaults = Me.ClassName.ClassFields
        Debug.Assert VarType(defaults) And vbArray = vbArray
        Debug.Assert (LBound(defaults, 1) = 0) And (UBound(defaults, 1) = 1)
        this.logPath = CStr(defaults(0))
        this.logName = CStr(defaults(1))
    End If
    this.logPathName = this.logPath & Application.PathSeparator & this.logName
    Set this.Base = BaseLogger.Create
End Sub


'@Description("Returns default values.")
Public Property Get ClassFields() As Variant
Attribute ClassFields.VB_Description = "Returns default values."
    Guard.NonDefaultInstance Me 'Must be called on the default instance
    ClassFields = Array(this.logPath, this.logName)
End Property


'@Description("Default factory")
Private Function ILogger_Create(Optional ByVal options As Variant = Empty) As ILogger
Attribute ILogger_Create.VB_Description = "Default factory"
    Set ILogger_Create = FileLogger.Create(options)
End Function


'@Description("Returns instance reference")
Private Property Get ILogger_Self() As ILogger
Attribute ILogger_Self.VB_Description = "Returns instance reference"
    Set ILogger_Self = Me
End Property


'@Description("Returns class reference")
Private Property Get ILogger_ClassName() As ILogger
Attribute ILogger_ClassName.VB_Description = "Returns class reference"
    Set ILogger_ClassName = FileLogger
End Property


'@Description("Returns private class fields. Must be called on the default instance.")
Private Property Get ILogger_ClassFields() As Variant
Attribute ILogger_ClassFields.VB_Description = "Returns private class fields. Must be called on the default instance."
    ILogger_ClassFields = FileLogger.ClassFields
End Property


'@Description("Writes a single line or an array to immediate")
Private Sub ILogger_WriteLines(ByVal lines As Variant, Optional ByVal indent As Long = 0)
Attribute ILogger_WriteLines.VB_Description = "Writes a single line or an array to immediate"
    Dim buffer As String
    If (VarType(lines) And vbArray) = vbArray Then
        buffer = Space(indent) + Join(lines, vbNewLine & Space(indent))
    Else
        buffer = Space(indent) + CStr(lines)
    End If
    this.Base.WriteToImmediate buffer
    
    this.fileHandle = FreeFile
    Open this.logPathName For Append As #this.fileHandle
    Print #this.fileHandle, buffer
    Close #this.fileHandle
End Sub
