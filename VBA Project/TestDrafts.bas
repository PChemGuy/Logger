Attribute VB_Name = "TestDrafts"
'@IgnoreModule AssignmentNotUsed, ProcedureNotUsed, VariableNotUsed, UseMeaningfulName
'@Folder("Drafts")

Option Explicit

'    AssertExpectedError ErrNo.PassedNoErr, Guard, "EmptyString", , "Non-empty string"

' Cannot use CallByName due to a bug in VBA 6 (Instead of the actual error information
' generated in the called method, Automation Error is returned. Per MS suggestion,
' calling TypeLib Information Objects is a necessary workaround.)
'
'''@Description("Verifies that the expected error was raised.")
''Private Sub AssertExpectedError( _
''        Optional ByVal ExpectedErrorNo As ErrNo = ErrNo.PassedNoErr, _
''        Optional ByVal ObjectVar As Object = Nothing, _
''        Optional ByVal Member As String = vbNullString, _
''        Optional ByVal CallType As VbCallType = VbMethod, _
''        Optional ByVal Args As Variant = Empty)
''
''    If (VarType(Args) And vbArray) <> 0 Then
''        VBA.Err.Raise ErrNo.NotImplementedErr, "AssertExpectedError - Testing", "Multiple arguments feature not yet implemented"
''    ElseIf Not ObjectVar Is Nothing Then
''        On Error Resume Next
''        CallByName ObjectVar, Member, CallType, Args
''    End If
''
''    Dim ActualErrNo As Long
''    ActualErrNo = VBA.Err.number
''    Dim ErrorDetails As String
''    ErrorDetails = " Error: #" & ActualErrNo & " - " & VBA.Err.description
''    VBA.Err.Clear
''
''    Select Case ActualErrNo
''    Case ExpectedErrorNo
''        Assert.Succeed
''    Case ErrNo.PassedNoErr
''        Assert.Fail MsgExpectedErrNotRaised
''    Case Else
''        Assert.Fail MsgUnexpectedErrRaised & ErrorDetails
''    End Select
''End Sub



Private Sub mmm()
    Dim qqq As Variant
    Set qqq = WorksheetLog
    qqq = Array(2, 3)
    WorksheetLog.Range("A1:A2").value = qqq
End Sub


